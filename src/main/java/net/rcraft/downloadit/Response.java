package net.rcraft.downloadit;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import net.rcraft.downloadit.task.FileDownloadTask;
import net.rcraft.downloadit.task.TransferTask;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.io.FileUtils;
import org.springframework.core.io.InputStreamResource;

import java.io.*;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by rdisrupter on 21.12.16.
 */
@Slf4j
public class Response implements Closeable {

    private static final String README = "readme.txt";

    @Getter
    private InputStreamResource resource;

    private PrintWriter readmeWriter;

    private ArchiveOutputStream archiveOutputStream;

    @Getter
    private WritableByteChannel archiveOutputChannel;

    @Getter
    private Path directory;

    private Request request;

    private Set<TransferTask> tasks = Collections.synchronizedSet(new HashSet<>());

    public static class Builder {

        private Optional<Integer> bufferSize;
        private Request request;


        public Response build() {

            Objects.requireNonNull(request, "Request is null");

            Path directory;
            OutputStream outputStream;
            ArchiveOutputStream archiveOutputStream;
            WritableByteChannel archiveOutputChannel;
            PrintWriter readmeWriter;
            InputStreamResource resource;

            try {
                directory = Files.createTempDirectory(null);
                log.debug("Create temp directory: {}", directory.toAbsolutePath().toString());
            } catch (IOException ex) {
                throw new DownloaditController.InternalServerError("Cant create tempory directory", ex);
            }

            try {
                PipedInputStream pipedInputStream = bufferSize.map(PipedInputStream::new)
                        .orElseGet(PipedInputStream::new);
                resource = new InputStreamResource(pipedInputStream);

                outputStream = new PipedOutputStream(pipedInputStream);
            } catch (IOException ex) {
                throw new DownloaditController.InternalServerError("Cant create output pipe stream", ex);
            }


            try {
                archiveOutputStream = new ArchiveStreamFactory()
                        .createArchiveOutputStream(ArchiveStreamFactory.TAR, outputStream);
            } catch (ArchiveException ex) {
                throw new DownloaditController.InternalServerError("Cant create archive for pipe stream", ex);
            }


            try {
                Path readme = directory.resolve(README);
                readmeWriter = new PrintWriter(FileUtils.openOutputStream(readme.toFile()));
                readmeWriter.println("=====================================================");
                readmeWriter.println("==================  Rcraft Net  =====================");
                readmeWriter.println("=====================================================");
                readmeWriter.println();
                readmeWriter.println("We've downloaded these files for you. Hope you ");
                readmeWriter.println("enjoy our service. Have a nice day and good luck!");
                readmeWriter.println();
                readmeWriter.println("Good luck!");
                readmeWriter.println();
                readmeWriter.println();
            } catch (IOException ex) {
                throw new DownloaditController.InternalServerError("Cant create readme file for write", ex);
            }

            archiveOutputChannel = Channels.newChannel(archiveOutputStream);

            return new Response(
                    request,
                    resource,
                    directory,
                    archiveOutputStream,
                    archiveOutputChannel,
                    readmeWriter
            );
        }

        public Builder setBufferSize(Integer bufferSize) {
            this.bufferSize = Optional.ofNullable(bufferSize);
            return this;
        }

        public Builder setRequest(Request request) {
            this.request = request;
            return this;
        }

    }

    private Response(
            Request request,
            InputStreamResource resource,
            Path tempDirectory,
            ArchiveOutputStream outputStream,
            WritableByteChannel outputChannel,
            PrintWriter readmeWriter
    ) {

        this.request = request;
        this.resource = resource;
        this.directory = tempDirectory;
        this.archiveOutputStream = outputStream;
        this.archiveOutputChannel = outputChannel;
        this.readmeWriter = readmeWriter;

    }

    public void addTask(TransferTask task) {
        log.debug("Add task {}", task);
        tasks.add(task);
        if (task instanceof FileDownloadTask) {
            countFiles.incrementAndGet();
        }
    }

    private ReentrantLock lock = new ReentrantLock();

    private AtomicInteger countFiles = new AtomicInteger(0);

    private volatile boolean available = true;

    private Queue<Path> preparedFiles = new LinkedList<>();

    public Optional<Path> onDownload(Path file) {
        lock.lock();
        try {

            if (!available) {
                preparedFiles.add(file);
                return Optional.empty();
            }

            Optional<Path> newFile = Optional.ofNullable(file);
            available = !newFile.isPresent();
            return newFile;

        } finally {
            lock.unlock();
        }
    }

    public Optional<Path> onUpload() {
        lock.lock();
        try {
            Optional<Path> nextFile = Optional.ofNullable(preparedFiles.poll());
            if (!nextFile.isPresent()) {
                available = true;
                if (request.isInputStreamClose()) {
                    if (countFiles.get() == 0) {
                        readmeWriter.flush();
                        return Optional.of(directory.resolve(README));
                    }
                    if (countFiles.get() == -1) {
                        close();
                    }
                }
            }
            return nextFile;
        } finally {
            lock.unlock();
        }
    }

    public WritableByteChannel startArchiveEntry(ArchiveEntry entry) {
        log.trace("Open new entry {}", entry);
        try {
            archiveOutputStream.putArchiveEntry(entry);
        } catch (IOException e) {
            log.error("Error to open entry", e);
            close();
        }
        return archiveOutputChannel;
    }

    public void endArchiveEntry() {
        log.trace("Close entry");
        try {
            archiveOutputStream.closeArchiveEntry();
        } catch (IOException e) {
            log.error("Error to close entry", e);
            close();
        }
        countFiles.decrementAndGet();
    }

    public void close() {
        try {
            log.debug("Close response");
            tasks.forEach(TransferTask::terminate);
            readmeWriter.close();
            FileUtils.deleteDirectory(directory.toFile());
            archiveOutputChannel.close();
            archiveOutputStream.close();
        } catch (IOException e) {
            log.error("Error to close response", e);
        }

    }

    public void downloadError(String url, Exception e) {
        log.debug("Download error, print exception");
        countFiles.decrementAndGet();

        readmeWriter.printf("SORRY we cant download this file =( %n");
        e.printStackTrace(readmeWriter);
        readmeWriter.println();
    }

}
