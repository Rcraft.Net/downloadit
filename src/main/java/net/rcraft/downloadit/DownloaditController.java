package net.rcraft.downloadit;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by rdisrupter on 19.12.16.
 */
@Controller
public class DownloaditController {

    @Autowired
    private DownloaditService service;

    @Autowired
    private ObjectMapper objectMapper;

    @RequestMapping(method = RequestMethod.POST, value = "/downloadit")
    @ResponseBody
    public Resource downloadIt(InputStream bodyStream) throws IOException {

        return service
                .prepareResponse(new Request(bodyStream, objectMapper))
                .getResource();

    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public static class InternalServerError extends RuntimeException {
        public InternalServerError(String message, Throwable cause) {
            super(message, cause);
        }
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public static class BadRequestError extends RuntimeException {
        public BadRequestError(String message, Throwable cause) {
            super(message, cause);
        }

        public BadRequestError(String message) {
            super(message);
        }
    }


}
