package net.rcraft.downloadit.task;

import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Path;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by rdisrupter on 20.12.16.
 */
@Slf4j
public class ArchiveTask extends TransferTask {

    private Path file;

    @Setter
    @NonNull
    private Runnable onUploadConsumer;

    @Setter
    @NonNull
    private Function<ArchiveEntry, WritableByteChannel> onStartEntryFunction;

    @Setter
    @NonNull
    private Runnable onEndEntryFunction;

    @Setter
    @NonNull
    private Consumer<Exception> onErrorConsumer;

    public ArchiveTask(Path file, int bufferSize) {
        super(bufferSize);
        this.file = file;
    }

    @Override
    public void start() {
        Objects.requireNonNull(onStartEntryFunction);
        Objects.requireNonNull(onEndEntryFunction);
        Objects.requireNonNull(onErrorConsumer);
        log.trace("Start: {}", this.toString());
        super.start();
    }

    @Override
    protected void prepareResources() throws IOException {
        log.trace("Prepare: {}", this.toString());
        TarArchiveEntry archiveEntry = new TarArchiveEntry(file.toFile(), file.getFileName().toString());
        archiveEntry.setSize(file.toFile().length());

        outputChannel = onStartEntryFunction.apply(archiveEntry);
        inputChannel = new FileInputStream(file.toFile()).getChannel();

    }

    @Override
    protected void onSuccess() {
        log.trace("Success: {}", this.toString());
        onEndEntryFunction.run();
        releaseResources();
        onUploadConsumer.run();
    }

    @Override
    protected void onError(Exception e) {
        log.trace("Error: {}", this.toString(), e);
        releaseResources();
        onErrorConsumer.accept(e);
    }

    @Override
    protected void onTerminate() {
        log.trace("Terminate: {}", this.toString());
        releaseResources();
    }

    private void releaseResources() {
        try {
            if (inputChannel != null)
                inputChannel.close();
        } catch (IOException e) {
            log.warn("Cant close input channel", e);
        }
    }

    @Override
    public String toString() {
        return "ArchiveTask{" + file.getFileName().toString() + '}';
    }
}
