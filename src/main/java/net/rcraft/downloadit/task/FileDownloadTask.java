package net.rcraft.downloadit.task;

import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by rdisrupter on 20.12.16.
 */
@Slf4j
public class FileDownloadTask extends TransferTask {

    private static final Pattern CONTENT_DISPOSITION_PATTERN = Pattern.compile(".*filename=\"(.*)\".*");

    private String url;
    private Path directory;
    private Path file;

    @Setter
    @NonNull
    private Consumer<Path> onDownloadConsumer;

    @Setter
    @NonNull
    private BiConsumer<String, Exception> onErrorBiConcummer;

    public FileDownloadTask(@NonNull Path directory, @NonNull String url, int bufferSize) {
        super(bufferSize);
        this.url = url;
        this.directory = directory;
    }

    @Override
    public void start() {
        Objects.requireNonNull(onDownloadConsumer);
        Objects.requireNonNull(onErrorBiConcummer);
        super.start();
    }

    @Override
    protected void prepareResources() throws IOException {

        URLConnection urlConnection = new URL(url).openConnection();

        InputStream stream = urlConnection.getInputStream();
        inputChannel = Channels.newChannel(stream);

        file = prepareFile(directory, urlConnection);

        outputChannel = new FileOutputStream(file.toFile()).getChannel();

    }

    @Override
    protected void onSuccess() throws IOException {
        log.trace("Success: {}", this.toString());
        releaseResources();
        onDownloadConsumer.accept(file);
    }

    @Override
    protected void onError(Exception e) {
        log.trace("Error: {}", this.toString());
        releaseResources();
        onErrorBiConcummer.accept(url, e);
    }

    @Override
    protected void onTerminate() {
        releaseResources();
    }

    private Path prepareFile(Path directory, URLConnection urlConnection) throws IOException {

        String filename = urlConnection.getURL().getFile().substring(urlConnection.getURL().getFile().lastIndexOf('/') + 1);

        if (filename.length() == 0) {
            filename = "noname";
        }

        String contentDisposition = urlConnection.getHeaderField("Content-Disposition");

        if (contentDisposition != null) {
            Matcher matcher = CONTENT_DISPOSITION_PATTERN.matcher(contentDisposition);
            if (matcher.matches()) {
                filename = matcher.group(1);
            }
        }

        int i = 1;

        Path file = directory.resolve(filename);
        while (true) {

            log.trace("Try create file with filename {}", filename);

            if (Files.notExists(file)) {
                try {
                    return Files.createFile(file);
                } catch (FileAlreadyExistsException ex) {
                    // unsuccessful, continue
                }
            }

            int indexOfPoint = filename.lastIndexOf('.');
            if (indexOfPoint != -1) {
                filename = filename.substring(0, indexOfPoint) + '_' + i++ + filename.substring(indexOfPoint);
            } else {
                filename = filename + '_' + i++;
            }
            file = directory.resolve(filename);
        }

    }

    private void releaseResources() {
        try {
            if (inputChannel != null)
                inputChannel.close();
        } catch (IOException e) {
            log.warn("Cant close input channel", e);
        }
        try {
            if (outputChannel != null) {
                outputChannel.close();
            }
        } catch (IOException e) {
            log.warn("Cant close output channel", e);
        }
    }

    @Override
    public String toString() {
        return "FileDownloadTask{" + url + '}';
    }
}
