package net.rcraft.downloadit.task;

import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * Created by rdisrupter on 20.12.16.
 */
@Slf4j
public abstract class TransferTask implements Runnable {

    protected ReadableByteChannel inputChannel;
    protected WritableByteChannel outputChannel;
    private final ByteBuffer buffer;

    private volatile boolean active = true;
    private volatile boolean prepearedResources = false;
    private volatile int downloadBytes = 0;

    @Setter
    @NonNull
    private Consumer<? super TransferTask> onContinueTaskConsumer;

    protected TransferTask(int bufferSize) {
        buffer = ByteBuffer.allocateDirect(bufferSize);
    }

    public void start() {
        Objects.requireNonNull(onContinueTaskConsumer);
        continueTask();
    }

    public void terminate() {
        log.trace("Terminate task {}", this.toString());
        active = false;
    }

    @Override
    public void run() {
        try {

            if (!active) {
                onTerminate();
                return;
            }

            if (!prepearedResources) {
                prepareResources();
                Objects.requireNonNull(inputChannel);
                Objects.requireNonNull(outputChannel);
                prepearedResources = true;
                continueTask();
                return;
            }

            int read = inputChannel.read(buffer);

            if (read == -1) {
                active = false;
                onSuccess();
                return;
            }

            buffer.flip();
            while (buffer.hasRemaining()) {
                outputChannel.write(buffer);
            }
            buffer.clear();

            this.downloadBytes += read;

            log.trace("[{}] Read: {}", this.toString(), downloadBytes);

            continueTask();

        } catch (Exception e) {
            active = false;
            onError(e);
        }

    }

    protected abstract void prepareResources() throws IOException;

    protected abstract void onSuccess() throws IOException;

    protected abstract void onError(Exception e);

    protected abstract void onTerminate();

    private void continueTask() {
        onContinueTaskConsumer.accept(this);
    }

}
