package net.rcraft.downloadit;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Created by rdisrupter on 20.12.16.
 */
@Data
@Slf4j
public class Request {

    private InputStream inputStream;

    private JsonFactory jsonFactory;

    private volatile boolean inputStreamClose;

    public Request(InputStream inputStream, ObjectMapper objectMapper) {
        this.jsonFactory = objectMapper.getFactory();
        this.inputStream = inputStream;
    }

    public Stream<String> streamUrls() {

        JsonParser parser;
        try {

            parser = jsonFactory.createParser(inputStream);
            JsonToken jsonToken = parser.nextToken();

            if (!parser.hasCurrentToken() || jsonToken != JsonToken.START_ARRAY) {
                throw new DownloaditController.BadRequestError("Json must contains array of urls");
            }

        } catch (IOException ex) {
            throw new DownloaditController.BadRequestError("Malformed json", ex);
        }

        return StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(
                        new JsonIterator(parser),
                        Spliterator.ORDERED | Spliterator.NONNULL | Spliterator.IMMUTABLE
                ),
                false
        );
    }


    private class JsonIterator implements Iterator<String> {

        JsonParser parser;
        JsonToken token;
        String next;

        public JsonIterator(JsonParser parser) {
            this.parser = parser;

        }

        @Override
        public boolean hasNext() {

            try {

                token = parser.nextToken();

                while (parser.hasCurrentToken()) {

                    if (token == JsonToken.VALUE_STRING) {

                        next = parser.getText();
                        log.trace("Parsed url {}", next);

                        return true;

                    } else if (token == JsonToken.END_ARRAY) {
                        inputStreamClose = true;
                        return false;
                    } else {
                        log.trace("Skip unrecognised token: {}", token.asString());
                        parser.skipChildren();
                    }
                    token = parser.nextToken();
                }
            } catch (IOException ex) {
                throw new DownloaditController.BadRequestError("Malformed json", ex);
            }

            return false;
        }

        @Override
        public String next() {
            return next;
        }
    }

}
