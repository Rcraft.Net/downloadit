package net.rcraft.downloadit;

import lombok.extern.slf4j.Slf4j;
import net.rcraft.downloadit.task.ArchiveTask;
import net.rcraft.downloadit.task.FileDownloadTask;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by rdisrupter on 20.12.16.
 */
@Component
@Slf4j
public class DownloaditService {

    private final static int BUFFER_SIZE = 16 * 1024;

    private ExecutorService executorService = Executors.newFixedThreadPool(
            Optional.ofNullable(System.getenv("WORKER_THREAD_COUNT"))
                    .map(Integer::parseInt)
                    .orElse(Runtime.getRuntime().availableProcessors())
    );

    public Response prepareResponse(Request request) {

        Response response = new Response.Builder()
                .setBufferSize(BUFFER_SIZE)
                .setRequest(request)
                .build();

        try {
            request.streamUrls()
                    .forEach(url -> startDownloadTask(response, url));

        } catch (Exception e) {
            response.close();
            throw e;
        }

        return response;

    }

    private void startDownloadTask(Response response, String url) {
        FileDownloadTask task = new FileDownloadTask(response.getDirectory(), url, BUFFER_SIZE);
        task.setOnContinueTaskConsumer(executorService::execute);
        task.setOnDownloadConsumer(file -> successDownload(response, file));
        task.setOnErrorBiConcummer(response::downloadError);
        response.addTask(task);
        task.start();
    }

    private void successDownload(Response response, Path file) {
        response.onDownload(file)
                .ifPresent((nextFile) -> {
                            startUploadTask(response, nextFile);
                        }

                );
    }

    private void startUploadTask(Response response, Path file) {
        ArchiveTask task = new ArchiveTask(file, BUFFER_SIZE);
        task.setOnContinueTaskConsumer(executorService::execute);
        task.setOnStartEntryFunction(response::startArchiveEntry);
        task.setOnEndEntryFunction(response::endArchiveEntry);
        task.setOnUploadConsumer(() -> response.onUpload()
                .ifPresent(nextFile -> startUploadTask(response, nextFile)));
        task.setOnErrorConsumer(exception -> {
            log.error("Error archive files, close", exception);
            response.close();
        });
        response.addTask(task);
        task.start();

    }
}
