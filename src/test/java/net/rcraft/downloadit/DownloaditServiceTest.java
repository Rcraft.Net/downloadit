package net.rcraft.downloadit;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.rcraft.downloadit.task.FileDownloadTask;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.utils.IOUtils;
import org.easymock.TestSubject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.api.easymock.annotation.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.powermock.api.easymock.PowerMock.expectNew;

/**
 * Created by rdisrupter on 23.12.16.
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest({URL.class, FileDownloadTask.class})
public class DownloaditServiceTest {

    private static final String TEST_STRING = "\"lkdnklancuyn87cyas8c9naus89cnus8cy78ywbe78cywe87cy by87bew87 nyew87y c78e 7c8ey\"";

    @TestSubject
    private DownloaditService service = new DownloaditService();

    @Mock
    private URL url;

    @Mock
    private URLConnection connection;

    @Mock
    private InputStream stream = new ByteArrayInputStream(TEST_STRING.getBytes());

    @Before
    public void setUp() throws Exception {
        expectNew(URL.class, "urlas")
                .andReturn(url)
                .anyTimes();

        expect(url.openConnection())
                .andReturn(connection)
                .anyTimes();

        expect(connection.getURL())
                .andReturn(url)
                .anyTimes();

        expect(connection.getInputStream())
                .andReturn(stream)
                .anyTimes();

        expect(url.getFile())
                .andReturn("/laja/asd.gif")
                .anyTimes();

        expect(connection.getHeaderField("Content-Disposition"))
                .andReturn(null)
                .anyTimes();
    }

    @Test
    public void testPrepareResponse() throws Exception {

        ByteArrayInputStream inputStream = new ByteArrayInputStream("[\"urlas\"]".getBytes());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        Request request = new Request(inputStream, new ObjectMapper());

        PowerMock.replay(url, connection, URL.class);

        InputStream responseStream = service.prepareResponse(request)
                .getResource()
                .getInputStream();

        PowerMock.verify(this.url, connection, URL.class);

        ArchiveInputStream tar = new ArchiveStreamFactory().createArchiveInputStream("tar", responseStream);

        ArchiveEntry entry;
        while ((entry = tar.getNextEntry()) != null) {
            if (entry.getName().equals("asd.gif")) {
                IOUtils.copy(tar, outputStream);
                assertEquals(TEST_STRING, outputStream.toString());
            } else {
                assertEquals(entry.getName(), "readme.txt");
            }
        }

    }
}