package net.rcraft.downloadit.task;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.api.easymock.annotation.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;
import static org.powermock.api.easymock.PowerMock.expectNew;

/**
 * Created by rdisrupter on 23.12.16.
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest({URL.class, FileDownloadTask.class})
public class FileDownloadTaskTest {

    @Mock
    private URL url;

    @Mock
    private URLConnection connection;

    @Mock
    private InputStream stream;

    private Path tempDirectory;


    @Before
    public void setUp() throws Exception {
        expectNew(URL.class, "urlas")
                .andReturn(url)
                .anyTimes();

        expect(url.openConnection())
                .andReturn(connection)
                .anyTimes();

        expect(connection.getURL())
                .andReturn(url)
                .anyTimes();
        expect(connection.getInputStream())
                .andReturn(stream)
                .anyTimes();

        tempDirectory = Files.createTempDirectory(null);

    }

    @After
    public void tearDown() throws IOException {
        FileUtils.deleteDirectory(tempDirectory.toFile());
    }

    @Test
    public void testCreateFile() throws Exception {

        expect(url.getFile())
                .andReturn("/laja/asd.gif")
                .anyTimes();

        expect(connection.getHeaderField("Content-Disposition"))
                .andReturn(null)
                .anyTimes();

        FileDownloadTask task = new FileDownloadTask(tempDirectory, "urlas", 1024);

        PowerMock.replay(url, connection, URL.class);

        task.prepareResources();

        PowerMock.verify(this.url, connection, URL.class);

        List<Path> files = Files.walk(tempDirectory, 1)
                .collect(Collectors.toList());
        assertEquals(2, files.size());
        assertEquals(files.get(1).getFileName().toString(), "asd.gif");
    }
}