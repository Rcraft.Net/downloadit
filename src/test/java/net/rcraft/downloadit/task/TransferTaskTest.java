package net.rcraft.downloadit.task;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Optional;

/**
 * Created by rdisrupter on 23.12.16.
 */
@Slf4j
public class TransferTaskTest {

    private static final String TEST_STRING = "\"lkdnklancuyn87cyas8c9naus89cnus8cy78ywbe78cywe87cy by87bew87 nyew87y c78e 7c8ey\"";

    @Test
    public void testTransfer() throws Exception {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(TEST_STRING.length());

        Task task = new Task();

        task.setOutputChannel(Channels.newChannel(outputStream));
        task.setInputChannel(Channels.newChannel(new ByteArrayInputStream(TEST_STRING.getBytes())));

        task.setOnContinueTaskConsumer(TransferTask::run);

        task.start();

        Assert.assertEquals(TEST_STRING, outputStream.toString());
    }

    @Setter
    private static class Task extends TransferTask {

        private Runnable onPrepare;
        private Runnable onSuccess;
        private Runnable onError;
        private Runnable onTerminate;

        protected Task() {
            super(1024);
        }

        public void setInputChannel(ReadableByteChannel channel) {
            inputChannel = channel;
        }

        public void setOutputChannel(WritableByteChannel channel) {
            outputChannel = channel;
        }

        @Override
        protected void prepareResources() throws IOException {
            Optional.ofNullable(onPrepare)
                    .ifPresent(Runnable::run);
        }

        @Override
        protected void onSuccess() throws IOException {
            Optional.ofNullable(onSuccess)
                    .ifPresent(Runnable::run);
        }

        @Override
        protected void onError(Exception e) {
            Optional.ofNullable(onError)
                    .ifPresent(Runnable::run);
        }

        @Override
        protected void onTerminate() {
            Optional.ofNullable(onTerminate)
                    .ifPresent(Runnable::run);
        }
    }

    ;
}