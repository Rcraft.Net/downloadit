# DownloadIt!
[![build status](https://gitlab.com/Rcraft.Net/downloadit/badges/master/build.svg)](https://gitlab.com/Rcraft.Net/downloadit/commits/master)[![coverage report](https://gitlab.com/Rcraft.Net/downloadit/badges/master/coverage.svg)](https://gitlab.com/Rcraft.Net/downloadit/commits/master)

## What is that ?

DownloadIt! is a free service created just for fun. 

## How it works?

You send a number of file URLs and receive those files neatly packed in a tar archive.

## Docker image

We have an official Docker Hub registry.
https://hub.docker.com/r/rcraftnet/download-server/

To start the service from Docker, run
> docker run -d -p 80:8080 rcraftnet/download-server

More information about server configuration and administration can be found on the official Docker page.

## Build

To build the jar and the docker-image run the command below
> mvn package

## Standalone run

To run the service outside the Docker container just execute
> java -Dlogging.config=/path/to/log4j2.xml
-jar downloadit.jar

## Request examle

> curl --data-ascii '["http://cdn.adnxs.com/p/eb/66/5d/2a/eb665d2ae426b7acab76776f316af95c.gif",
"https://pp.vk.me/c9835/g27786135/a_577cff0d.jpg", 
"http://demotivation.me/images/20100417/353c64vkfxx1.jpg", 
"http://apikabu.ru/img_n/2012-08_1/1zy.jpg", 
"http://imgdiscover.ru/i/2/wAaodj.jpg", 
"http://rusdemotivator.ru/uploads/10-13-11/1318530671-kotye..jpg", 
"https://files7.adme.ru/files/news/part_76/762760/8631410-R3L8T8D-650-980x-8.jpg", 
"http://s.pikabu.ru/post_img/2013/09/12/6/1378972461_2112391706.jpeg", 
"http://novostei.net/uploads/posts/2013-02/1359987594_3289_tolko-poprobuj_demotivators_ru.jpg", 
"http://demotivators.to/media/posters/346/717229_poglad-kote.jpg", 
"https://pp.vk.me/c562/u24278059/124012697/x_2a3c656d.jpg", 
"http://www.pozzzy.ru/Demotiv/demokot/k1.aspx", 
"http://kot3.do.am/_ph/1/2/623346859.jpg", 
"http://www.mixnews.lv/uploads/comments/comments_35710ebc51a11f6d2147aa31501bf8e54ef4b68e.jpg", 
"http://demotivation.me/images/20100325/ik4ywtnqa03m.jpg", 
"http://www.mrwolf.ru/images/upload/11093-1.jpg", 
"http://rusdemotivator.ru/uploads/12-08-2012/2012120818153467.png", 
"http://apikabu.ru/img_n/2012-09_3/4u1.jpg", 
"http://s00.yaplakal.com/pics/pics_original/7/4/0/7752047.jpg", 
"https://pp.vk.me/c9767/u153057869/136391758/x_25485f80.jpg", 
"http://rusdemotivator.ru/uploads/posts/2012-12/1356904030_13937077_cat7z.jpg",
"http://rusdemotivator.ru/uploads/01-07-12/1325924618-nano-kotye.jpg",
"http://apikabu.ru/img_n/2011-09_6/do2.jpg",
"http://www.sunhome.ru/UsersGallery/062012/milii-kote.jpg",
"http://demotivators.to/media/posters/938/38268_staryij-mudryij-kote.jpg",
"http://bm.img.com.ua/img/prikol/images/large/2/9/152292_267542.jpg",
"https://files4.adme.ru/files/news/part_76/762760/preview-650x341-98-1457959353.jpg",
"http://lurkmore.so/images/thumb/1/14/Kote.jpg/180px-Kote.jpg",
"http://www.stihi.ru/photos/katerashe.jpg","https://pp.vk.me/c306315/v306315992/a311/byj4DWuQWoM.jpg",
"http://bygaga.com.ua/uploads/posts/1344357846_demi_s_jivotnimi_ot_bygaga_8879-17.jpg",
"http://demotivation.ru/images/20101125/xj4z1jkun22i.jpg",
"http://rusdemotivator.ru/uploads/posts/2013-05/1367943894_60186973_kote-karla-marksa.jpg"]'
--header "Content-type: application/json" -o "answer.tar" http://localhost:8080/downloadit

